/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bioinf.nl.teamprimer.kvalidator;

import java.util.ArrayList;
import java.util.List;

/**
 * DONE:
 *      
 *      absence of hairpins >3bp
 *      Restrict homopolymers (runs > 4) 
 *      Restrict dimers ( > 4)
 *      Restrict minimum and/or maximum melting temperature
 * 
 * @author dcdekker
 */
public class PrimerChecker {
    List<String> sequences = new ArrayList<>();
    
    public static void main(String [] args){
        PrimerChecker pm = new PrimerChecker();
        pm.makeSomeTestSequences();
        pm.TestSequences();
    }    
        
    private void makeSomeTestSequences() {
        // test sequences of length 12
        sequences.add("AAACCCTTTGGG");
        sequences.add("AAAAACCCCCAA");
        sequences.add("AAAAAAAAAAAA");
        sequences.add("CCCCCCCCCGGG");
        sequences.add("ATATATCGCGCG");
        // test sequences of length 14
        sequences.add("AAAATTTTCCCCGG");
        sequences.add("AAAAACCCCCGGAA");
        sequences.add("CCCCCGGGGGGGTT");
        sequences.add("ATCCCGCGAACCCG");
        sequences.add("CAACGCAACGCGAA");
        
    }
    
    private void TestSequences() {
        
        for (int i = 0; i < sequences.size(); i++) {
               boolean answer1 = restrictDimers(sequences.get(i));
               boolean answer2 = restrictHairpins(sequences.get(i));
               boolean answer3 = restrictHomopolymers(sequences.get(i));
               
               //System.out.println( " hairpins is "+ answer2);
                
        }
       
    }
   
   /**
    * Checks the sequence for homopolymers
    * @param sequence
    * @return false if the sequence contains homopolymer(s) longer than 4
    */
    public static Boolean restrictHomopolymers(String sequence){
        int limit = 5;
        String regex = "[ACTG]*([A]{"+ limit +",}|[C]{"+ limit+ ",}|[T]{"+limit+",}|[G]{"+limit+",})[ATCG]*";
        if(sequence.matches(regex)){
           return false ;
        }
        else{
            return true;
        }
        
    }
    /**
     * Checks the sequence for dimers of length longer than 4
     * @param sequence
     * @return false if the sequence contains homopolymer(s) longer than 4
     */
    public static Boolean restrictDimers(String sequence){
        
        String regex = "[ACTG]*([ACTG]{2})\\1{4}[ACTG]*";
        if(sequence.matches(regex)){
           return false ;
        }
        else{
            return true;
        }
        
    }
    
    
    /**
     *  
     * Makes all possible subsequences from the sequence + the hairpin loop
     * possible in a hairpin construction, and compares them.
     * Returns a true if none of the subsequences matched.
     * @param sequence
     * @return true if no hairpins are present 
     */
    public static boolean restrictHairpins(String sequence){
        String subsequence1;
        String subsequence2;

        int maxHairpinLength = sequence.length() -6;
        
        for(int i = 0; i< sequence.length()-(maxHairpinLength+2); i++){           //loops through the sequence
            
            for(int m = 4; m<sequence.length(); m++){                            // creates the minimum to maximum length hairpin possibilities 
                if(i+2 + m +3 < sequence.length()){                             //limits the lengthening of hairpins by the lenght of the sequence
                    subsequence1 = sequence.substring((i),(i+3));
                    subsequence2 = sequence.substring((i+3+m), (i+3+m+3));
                    
                    if(checkComplement(subsequence1,subsequence2) == true){
                       return false;
                       
                    }
                }
                
            }
            
        }
        
        return true;
    }
    /**
     * Checks if the sequence is a reverse complement of the first sequence
     * Could someone perhaps make this a more efficient function? run-time is increased a lot by this!!
     * @param sequence
     * @param possibleComplement
     * @return true if sequence is a reverse compliment 
     */
    private static boolean checkComplement(String sequence, String possibleComplement){
       sequence = sequence.toUpperCase();
       StringBuilder comp = new StringBuilder();
       
       for(int i = 2; i>= 0 ;i-- ){           
           if(sequence.charAt(i) == 'A'){
               comp.append("T");
           }
           else if(sequence.charAt(i) == 'T'){
               comp.append("A");
           }
           else if(sequence.charAt(i) == 'G'){
               comp.append("C");
           }
           else if(sequence.charAt(i) == 'C'){
               comp.append("G");
           }
       }
       String complement = comp.toString();
       if(possibleComplement.equals(complement)){
           return false;
       }else{
           return true;
       }
       
    }
    
            /**
        * Translate the sequence to make it complements form.
        * @param sequence
        * @return comSequence
        */
    public String makeComplement(String sequence){
        StringBuilder comSequence = new StringBuilder();
        for (int i = 0; i< sequence.length(); i++){
        if(sequence.charAt(i) == 'T'){
            comSequence.append('A');
        }
        if(sequence.charAt(i) == 'A'){
            comSequence.append('T');
        }
        if(sequence.charAt(i) == 'C'){
            comSequence.append('G');
        }
        if(sequence.charAt(i) == 'G'){
            comSequence.append('T');}
        
        }
        System.out.println("Old:" + sequence);
        System.out.println("New:"+ comSequence);
        
        return comSequence.toString();
    }
    

    
    
    
    
}


