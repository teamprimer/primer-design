/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import java.io.IOException;
import java.util.Enumeration;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Sam van Haren
 */
public class CompressedFileReader {
    private final String zipFileLocation;
    private final ZipFile zipFile;
    private final Enumeration<? extends ZipEntry> entries;
    public CompressedFileReader(String zipLocation) throws IOException{
        this.zipFileLocation = zipLocation;
        this.zipFile = new ZipFile(this.zipFileLocation);
        this.entries = this.zipFile.entries();

    }
    public void OpenZip() throws IOException{
        System.out.println(entries.toString());
    }
    public void CloseZip() throws IOException {
        this.zipFile.close();
    }

}
