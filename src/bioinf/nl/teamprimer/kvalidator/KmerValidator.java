/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import java.util.ArrayList;
/**
 * so far has the functions:
 * 
 * restrict maximum and/or minimum melting temperature
 * restrict sequence on whether they contain a CG-clamp
 * Make a function for sequence diversity (% of AT and %GC)
 * Melting temperature between 40-65C
 * 
 */

/**
 * TODO: 
 * 
 * Make a Function for maximum palindrome length
 * 
 * Get Input from User
 * 
 * @author dcdekker
 */
public class KmerValidator {
    ArrayList<String> sequenceList = new ArrayList<>();
    
    public static void main(String [] args){
        KmerValidator k = new KmerValidator();
        k.addSomeSequences();
        //k.getRemainingSequences();
        k.restrictPalindromeLength("", 2);
    }
    /**
     * adds some test sequences
     */
    private void addSomeSequences(){
        // Using length of 12
        sequenceList.add("AAACCCTTTGGG");
        sequenceList.add("ACACACACACAC");
        sequenceList.add("TTTTTTTCCCCC");
        sequenceList.add("ACTGGAACTGGA");
        sequenceList.add("AACCTTGGGAAA");
        
        
    }
    /**
     * Removes any sequences from the arrayList which aren't relevant
     */
    private void getRemainingSequences(){
        // TODO get input from user
        
        for(int i = 0; i< sequenceList.size(); i++){
            
            if(!restrictMaximumMeltingTemperature(sequenceList.get(i),60)){
                sequenceList.remove(i);
            }
            if(!restrictMinimumMeltingTemperature(sequenceList.get(i),30)){
                sequenceList.remove(i);
            }
            if(!restrictSequenceDiversity(sequenceList.get(i),30)){
                sequenceList.remove(i);
            }
            
           
           
        }
       for(String string : sequenceList){
           System.out.println(string);
       }
 
    }
    /**
     * Makes subsequences from both ends of the sequence,shortening the sequence each time
     * checks if the reverse matches(palindrome)
     * returns a false if the longest palindrome's length found is below the given maximum
     * @param sequence
     * @param maxPalindromeLength
     * @return true if palindrome length is below the given maxiumum
     */
    private boolean restrictPalindromeLength(String sequence,int maxPalindromeLength){
        int palindromeLength = 0;
        int j;
        
        for(int i =0; i<sequence.length();i++){
            j = sequence.length()-1-i;
            
            StringBuilder sub = new StringBuilder();
            sub.append(sequence.substring(i, sequence.length()));
            StringBuilder sub2 = new StringBuilder();
            sub2.append(sequence.substring(0,j));
                        
            if(sub2.toString().matches(sub2.reverse().toString())){             
                if(sub2.length() > palindromeLength){
                    palindromeLength = sub.length();
                }
            }
            
            if(sub.toString().matches(sub.reverse().toString())){
                if(sub.length() > palindromeLength){
                    palindromeLength = sub.length();
                }
            }
        }
        if(palindromeLength > maxPalindromeLength){
                return false;
            }else{
                return true;
            }
        
    }
    
    /**
     * Calculates the percentage of AT based on occurrences
     * returns a true if the percentage given matches, otherwise a false
     * @param sequence
     * @param diversity
     * @return true if the AT percentage matches  
     */
    private boolean restrictSequenceDiversity(String sequence,double diversity){
        
        double ATCount = 0;
        for(int i = 0; i <sequence.length();i++){
            if(sequence.charAt(i) == 'A' | sequence.charAt(i) == 'T'){
                ATCount++;
            }
        }
        
        double sequenceDiversity = (ATCount/sequence.length())*100;
        
        if(sequenceDiversity == diversity){
            return true;
        }else{
            return false;
        }
        
    }
    
    private boolean restrictMinimumMeltingTemperature(String sequence,double minMeltTemp){
        double meltTemp = calculateMeltingTemperature(sequence);
        return meltTemp <= minMeltTemp;
        
    }
    private boolean restrictMaximumMeltingTemperature(String sequence,double maxMeltTemp){
        double meltTemp = calculateMeltingTemperature(sequence);
        return meltTemp <= maxMeltTemp;
    }
    
     /**
     * This program tests for a CG clamp
     * Checks if the last 5 bases contain 1 to 3 C or G's 
     * returns false on all other options
     * @param sequence
     * @return true if 1 to 3 C or G's have been found
     */
    public Boolean checkCGClamp(String sequence){
        
       String sub = sequence.substring(sequence.length()-5);
        System.out.println(sub);
        if(sub.matches("[ACTG]*([CG]{4}|[AT]{5})[ACTG]*")){
            return false;
        }else{
            return true;
        }
        
    }
    
    /**
     * Calculates the melting temperature based
     * on one rule or the other depending on the length of sequence
     * source: http://www.basic.northwestern.edu/biotools/oligocalc.html
     * @param sequence
     * @return melting temperature
     */
    private double calculateMeltingTemperature(String sequence){
        int countA = 0;
        int countC = 0;
        int countG = 0;
        int countT = 0;
        double meltTemp;
        for(int i = 0; i < sequence.length(); i++){
            if(sequence.charAt(i) == 'A'){
               countA ++;
            }
            if(sequence.charAt(i) == 'C'){
               countC ++;
            }
            if(sequence.charAt(i) == 'T'){
               countT ++;
            }
            if(sequence.charAt(i) == 'G'){
               countG ++;
            }
        }
        if(sequence.length() < 14){
           meltTemp = (countA + countT)*2 + (countG+countC)*4;
        }else{
           meltTemp = 64.9 + 41*(countG+countC-16.4)/(countA+countT+countG+countC);
        }
        
        return meltTemp;
    }
    
    
    
}
    
    
    
     
    

