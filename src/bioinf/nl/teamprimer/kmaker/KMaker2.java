/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bioinf.nl.teamprimer.kmaker;

import java.util.ArrayList;
import java.util.List;
import bioinf.nl.teamprimer.kvalidator.PrimerChecker;

/**
 *
 * @author nmkeur
 */
public class KMaker2 {
    public static void main(String[] args){
        KMaker2.createKmer();
    }
    
    public static void createKmer(){
        List<String> KmerList = new ArrayList<>();
        
        KmerList.add("A");
        KmerList.add("C");
        
        /**
         * this is how you change a magic number into a number.
         */
        int wall = Integer.parseInt("10");
        int i = KmerList.get(0).length();        
        while (i != 0) {
            List<String> KpmerList = new ArrayList<>();
            List<String> tmp1 = new ArrayList<>();
            if (i > wall) {
                    i = 0;
            } else {
            for (String p : KmerList) {
                KpmerList.add(p + "A");
                KpmerList.add(p + "C");
                KpmerList.add(p + "G");
                KpmerList.add(p + "T");
            tmp1 = new ArrayList<>(KpmerList);
                for (String s : KpmerList){
                    
                    if (!PrimerChecker.restrictHomopolymers(s)){
                        tmp1.remove(s);
                    }
                    if (!PrimerChecker.restrictDimers(s)){
                        tmp1.remove(s);
                    }
                    if (!PrimerChecker.restrictHairpins(s)){
                        tmp1.remove(s);
                    }
                    
                }
            }
            
                KmerList = tmp1;
                KpmerList = tmp1;
                System.out.println( "Kmer"+ KmerList);
                //KmerList = KmerList2;
                System.out.println(i);
                System.out.println(KmerList.size());
                i = KmerList.get(KmerList.size() -1).length();
                
            }
            //KmerList = KmerList2;
        }
        
    }
}
