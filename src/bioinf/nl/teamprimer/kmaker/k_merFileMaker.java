/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kmaker;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;

/**
 *
 * @author Sam van Haren
 */
public class k_merFileMaker {
    private PrintWriter writer;
    /**
     * opens a file with the name of the given sequence, for writing.
     * @param fileName for the name of the opened file
     */
    public final void fileOpen(final String fileName) {
        try {
            this.writer = new PrintWriter( fileName+".txt", "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("problem!");
        }
    }
    /**
     * closes the opened file
     */
    public final void fileClose() {
        this.writer.close();
    }
    /**
     * writes data to the opened file
     * @param data is a text sequence that can be written to the file
     */
    public final void fileWrite(final String data) {
        this.writer.println(data);
        }
}
