/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kmaker;

import bioinf.nl.teamprimer.kvalidator.PrimerChecker;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
/**
 *
 * @author Sam van Haren
 */
public class KMaker {
    /**
     * @param args the command line arguments
     * @throws java.io.IOException
     */
    public static void main(final String[] args) throws IOException {
        /**
         * Initialize Function KStart to create Kmers.
         * and Write to File
         */
        KMaker k = new KMaker();
        List<String> Kmers = k.KStart();
        k.firstWrite(Kmers);
        k.OtherKmers();
    }
    /**
     *Creates a list with possible combinations of Kmers till length 9.
     * And uses filters on this List to thin out the amount of Kmers.
     * @return list of Kmers.
     */
    public List<String> KStart() {
        List<String> KmerList = new ArrayList<>();                              //Initialize List
        KmerList.add("A");                                                      // Only make the combinations for the start nucleotide "A", "C"
        KmerList.add("C");                                                      // We can use the makeComplement function to get the other combinations
        int wall = Integer.parseInt("8");                                       //Max number of Kmers
        int i = KmerList.get(0).length();
        while (i != 0) {
            List<String> KpmerList = new ArrayList<>();
            List<String> tmp1 = new ArrayList<>();                              //Initialize tmp list
            if (i > wall) {
                i = 0;
            } else {
                for (String p : KmerList) {                                     // Adds the nucleotides to the List.
                    KpmerList.add(p + "A");                                     // Creating all possible combinations.
                    KpmerList.add(p + "C");
                    KpmerList.add(p + "G");
                    KpmerList.add(p + "T");
                }
                tmp1 = new ArrayList<>(KpmerList);                              //Create a copy of kpmerList
                for (String s : KpmerList) {                                    // Removes the sequences that are false
                    if (!PrimerChecker.restrictHomopolymers(s) || !PrimerChecker
                            .restrictDimers(s) || !PrimerChecker
                                    .restrictHairpins(s)) {
                        tmp1.remove(s);
                    }
                }
            KmerList = tmp1;                                                    // Updates the list with the filtered List.
            KpmerList = tmp1;
            //System.out.println(KmerList.size());
            i = KmerList.get(KmerList.size() - 1).length();                     // Updates the length based on last element in list
                }
        }
    return KmerList;
    }
    /**
     *Starts writing to the first file of length 10 Kmers using length 9.
     * This also filters out Kmers just like in Kstart().
     * @param Kmer An Arraylist of Kmers of length 9.
     */
    public final void firstWrite(final List<String> Kmer) {
        List<String> kmerFiles = new ArrayList<>();
        k_merFileMaker z = new k_merFileMaker();
        String fileLocation = "/commons/student/2014-2015/Thema10/svanharen/";
        z.fileOpen(fileLocation + Integer.toString(10));
        for (String s : Kmer) {
            if (PrimerChecker.restrictHomopolymers(s + "A")
                || PrimerChecker.restrictDimers(s + "A")
                || PrimerChecker.restrictHairpins(s + "A")) {
                    z.fileWrite(s + "A");
            }
            if (PrimerChecker.restrictHomopolymers(s + "C")
                || PrimerChecker.restrictDimers(s + "C")
                || PrimerChecker.restrictHairpins(s + "C")) {
                    z.fileWrite(s + "C");
            }
            if (PrimerChecker.restrictHomopolymers(s + "G")
                || PrimerChecker.restrictDimers(s + "G")
                || PrimerChecker.restrictHairpins(s + "G")) {
                    z.fileWrite(s + "G");
            }
            if (PrimerChecker.restrictHomopolymers(s + "T")
                || PrimerChecker.restrictDimers(s + "T")
                || PrimerChecker.restrictHairpins(s + "T")) {
                    z.fileWrite(s + "T");
            }
        }
        kmerFiles.add(fileLocation + Integer.toString(10) + ".txt");
        z.fileClose();
        kmerCompress("/commons/student/2014-2015/Thema10/svanharen/"
                + Integer.toString(10) + ".zip", kmerFiles);
    }
    /**
     * Creates all the other Kmer Files using the files made before.
     * Starts by using the 10.txt made in firstWrite(), then follows with
     * using 11.txt when making 12.txt, in each file there are Kmers of the
     * given length.
     * @throws IOException for when missing input or output.
     */
    public final void OtherKmers() throws IOException{
        int[] numbers = new int[6];
        numbers[0] = 11;
        numbers[1] = 12;
        numbers[2] = 13;
        numbers[3] = 14;
        numbers[4] = 15;
        numbers[5] = 16;
        String fileLocation = "/commons/student/2014-2015/Thema10/svanharen/";
        for (int i : numbers) {
            List<String> kmerFiles = new ArrayList<>();
            int readNumber = i - 1;
            k_merFileMaker z = new k_merFileMaker();
            int part = 1;
            String lengthNumber = Integer.toString(i);
            String kmerFile = fileLocation + lengthNumber
                + Integer.toString(part) + ".txt";
            kmerFiles.add(kmerFile);
            String zipFile = fileLocation + lengthNumber + ".zip";
            int counter = 0;
            z.fileOpen(fileLocation + lengthNumber + Integer.toString(part));
            String[] listOfFiles = new File(fileLocation).list();
            for (String file : listOfFiles) {
                if (file.endsWith("txt")
                        && file.startsWith(Integer.toString(readNumber))) {
                    String readFile = fileLocation + file;
                    try (BufferedReader br = new BufferedReader(
                        new FileReader(readFile))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (PrimerChecker.restrictHomopolymers(line + "A")
                                || PrimerChecker.restrictDimers(line + "A")
                                || PrimerChecker.restrictHairpins(line + "A")) {
                                    z.fileWrite(line + "A");
                            }
                            if (PrimerChecker.restrictHomopolymers(line + "C")
                                || PrimerChecker.restrictDimers(line + "C")
                                || PrimerChecker.restrictHairpins(line + "C")) {
                                    z.fileWrite(line + "C");
                            }
                            if (PrimerChecker.restrictHomopolymers(line + "G")
                                || PrimerChecker.restrictDimers(line + "G")
                                || PrimerChecker.restrictHairpins(line + "G")) {
                                    z.fileWrite(line + "G");
                            }
                            if (PrimerChecker.restrictHomopolymers(line + "T")
                                || PrimerChecker.restrictDimers(line + "T")
                                || PrimerChecker.restrictHairpins(line + "T")) {
                                    z.fileWrite(line + "T");
                            }
                            counter += 4;
                            if (counter >= 265241152) {
                                counter = 0;
                                z.fileClose();
                                part += 1;
                                kmerFile = fileLocation + lengthNumber
                                    + Integer.toString(part) + ".txt";
                                z.fileOpen(fileLocation + lengthNumber
                                    + Integer.toString(part));
                                kmerFiles.add(kmerFile);
                            }
                        }
                    }
                    z.fileClose();
                }
            }
            kmerCompress(zipFile, kmerFiles);
        }
    }

    /**
     * Compresses a single file into a zip file to make storage usage less.
     * @param zipFile name of the zip file.
     * @param txtFiles name of the file that is going to be compressed.
     */
    public final void kmerCompress(final String zipFile, final List<String> txtFiles) {
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                byte[] buffer = new byte[1024];
                try (ZipOutputStream zos = new ZipOutputStream(fos)) {
                    //System.out.println(txtFile);
                    for (String txtFile : txtFiles) {
                        ZipEntry ze = new ZipEntry(txtFile);
                        zos.putNextEntry(ze);
                        try (FileInputStream in =
                            new FileInputStream(txtFile)) {
                                int len;
                                while ((len = in.read(buffer)) > 0) {
                                    zos.write(buffer, 0, len);
                                }
                        }
                        zos.closeEntry();
                    }
                }
            } catch (FileNotFoundException ex) {
            Logger.getLogger(KMaker.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KMaker.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
