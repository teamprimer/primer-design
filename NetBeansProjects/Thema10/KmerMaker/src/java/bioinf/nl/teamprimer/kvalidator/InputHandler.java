/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
/**
 *
 * @author dcdekker
 */
public class InputHandler {
    static FileWriter writer = new FileWriter();

    public static void main(final String[] args) {
        InputHandler h = new InputHandler();
        HashMap parameters = new HashMap();
        parameters = h.getParameters();
        System.out.println(writer.getFileLocation());
        h.runInputHandler(parameters);
        String s = writer.getZipFileLocation();
        System.out.println(s);
    }

    /**
     * Loops through the sequences from file.
     * and parameters given from the website
     * If all checks have returned true, a file with sequences will be printed,
     * all of the files will be compressed into a zip file
     * @param parameters hashmap
     */
    public final void runInputHandler(final HashMap parameters) {

        String fileLength = parameters.get("length").toString();
        // create the first file
        writer.createFile(fileLength);
        // create List for the file names
        ArrayList<String> fileList = new ArrayList<>();
        // amount of answers you want eventually
        int maxAnswers = getMaxAnswers(parameters) - 1;
        CompressedFileReader cFR = readFileSequences(parameters);
        byte stop = 0;
        // set 2 counters for file writing uses
        int characterCounter = 0;
        int secondCharacterCounter = 0;
        while (stop != 1) {
            try {
                String sequence = cFR.giveLine();
                if (sequence != null) {
                    int rightAnswer = 0;
                    //loops through the parameters
                    Iterator it = parameters.entrySet().iterator();
                    while (it.hasNext()) {
                        Map.Entry pairs = (Map.Entry) it.next();
                        // checks if parameter has been given
                        if (pairs.getValue().toString().length() != 0) {
                            if (maxAnswers + 1 == 1) {
                               startPrinting(sequence, fileLength,
                                       characterCounter, secondCharacterCounter,
                                       fileList);
                               //run the restrictions
                            } else if (runRestrictions(
                                    pairs.getKey().toString(),
                                    pairs.getValue().toString(),
                                    sequence) == true) {
                                rightAnswer++;
                                // if all answers have been true, write to file
                                if (rightAnswer == maxAnswers) {
                                    startPrinting(sequence, fileLength,
                                    characterCounter, secondCharacterCounter,
                                    fileList);
                                }
                            }
                        }
                    }
                } else {
                    stop = 1;
                }
            } catch (IOException ex) {
                Logger.getLogger(InputHandler.class.getName()
                ).log(Level.SEVERE, null, ex);
            }
        }
        writer.closeFile(fileLength);
        fileList.add(writer.getFileLocation() + "kmers_of_length_"
                + fileLength);
        // compress the files
        writer.compressFiles(fileLength, fileList);
        // delete the compressed files <-- aka deleting the txt files.
        for (String f : fileList) {
            File f1 = new File(f);
            f1.delete();
        }

    }
    /**
     * Writes sequence to the file.
     * Opens a new file when/if the limit has been reached.
     * @param sequence String
     * @param fileLength String
     * @param characterCounter integer
     * @param secondCharacterCounter integer
     * @param fileList ArrayList<String>
     */
    public final void startPrinting(final String sequence,
            final String fileLength, int characterCounter,
            int secondCharacterCounter,
            final ArrayList<String> fileList) {

        writer.writeToFile(sequence);
        //counts the characters used
        characterCounter += Integer.parseInt(fileLength);
        //265241152 is maximum amount of primers.
        //will print a new file after maximum
        if (characterCounter >= 265241152) {
            secondCharacterCounter++;
            writer.writeNextFile(fileLength,
                    secondCharacterCounter);
            characterCounter = 0;
            fileList.add(
                    writer.createCompleteFileName(
                            fileLength, secondCharacterCounter));
        }
    }

    /**
     * @param parameters description
     * @return a Compressed File Reader.
     */
    public final CompressedFileReader readFileSequences(
            final HashMap parameters) {
        String length = parameters.get("length").toString() + ".zip";
        String zipMap = "/commons/student/2014-2015/Thema10/svanharen/";
        String zipName = zipMap + length;
        try {
            CompressedFileReader cFR = new CompressedFileReader(zipName);
            return cFR;
            // Should read in a file
        } catch (IOException ex) {
            Logger.getLogger(InputHandler.class.getName()).log(
                    Level.SEVERE, null, ex);
            return null;
        }

   }

    /**
     * Just for testing.
     * @return 
     */
    private HashMap getParameters() {
        HashMap hash = new HashMap();
        hash.put("length", "10");
        hash.put("mintemp", "35");
        return hash;

    }

    /**
     * Runs one validator for given parameter. Returns a true if the sequence
     * has passed the validation
     *
     * @param parameter String description
     * @param value String description
     * @param sequence description
     * @return boolean description
     */
    public final boolean runRestrictions(final String parameter,
            final String value, final String sequence) {

        KmerValidator kv = new KmerValidator();
        if (!parameter.equals("length")) {
            if (parameter.equals("diversity")) {
                double diversity = stringToDouble(value);
                if (kv.restrictSequenceDiversity(sequence, diversity)) {
                    return true;
                }
            } else if (parameter.equals("palindrome")) {
                int palindromeLength = Integer.parseInt(value);
                if (kv.restrictPalindromeLength(sequence, palindromeLength)) {
                    return true;
                }
            } else if (parameter.equals("mintemp")) {
                double mintemp = stringToDouble(value);
                if (kv.restrictMinimumMeltingTemperature(sequence, mintemp)) {
                    return true;
                }
            } else if (parameter.equals("maxtemp")) {
                double maxtemp = stringToDouble(value);
                if (kv.restrictMaximumMeltingTemperature(sequence, maxtemp)) {
                    return true;
                }
            } else if (parameter.equals("primerdimer")) {
                int primerDimerLimit = Integer.parseInt(value);
                if (kv.restrictPrimerDimer(sequence, primerDimerLimit)) {
                    return true;
                }
            } else if (parameter.equals("homopolymers")) {
                int homopolymersLimit = Integer.parseInt(value);
                if (kv.restrictHomoPolymers(sequence, homopolymersLimit)) {
                    return true;
                }
            } else if (parameter.equals("gcclamp")) {
                if(value.equals("yes")){
                    if (kv.checkCGClamp(sequence)) {
                        return true;
                    }
                }
            } else {
                System.out.println("Unsupported parameter!");
                return false;
            }
        }
        return false;
    }


    /**
     * Casts a string to a double.
     *
     * @param number description
     * @return digit
     */
    public final double stringToDouble(final String number) {
        double digit = Double.parseDouble(number);
        return digit;
    }

    /**
     * for loop to determine how many parameters have been used.
     *
     * @param parameters description
     * @return answers
     */
    private int getMaxAnswers(final HashMap parameters) {
        int answers = 0;
        Iterator it = parameters.entrySet().iterator();
        while (it.hasNext()) {
            Map.Entry pairs = (Map.Entry) it.next();
            if (!pairs.getValue().equals("")) {
                answers++;
            }
        }
        return answers;
    }



}
