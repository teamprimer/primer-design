/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package bioinf.nl.teamprimer.servlet;

import bioinf.nl.teamprimer.kvalidator.FileWriter;
import bioinf.nl.teamprimer.kvalidator.InputHandler;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 *
 * @author nmkeur
 */
public class Submit extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(final HttpServletRequest request,
            final HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
        
        HashMap hash = new HashMap();
        ArrayList<String> parameters = new ArrayList<>();
        parameters.add("length");
        parameters.add("palindrome");
        parameters.add("diversity");
        parameters.add("mintemp");
        parameters.add("maxtemp");
        parameters.add("primerdimer");
        parameters.add("homopolymers");
        parameters.add("gcclamp");

        
        String urrl = "http://bioinf.nl/~nmkeur/data/kmers_of_length"+request.getParameter("length")+".zip";
        for (String param : parameters) {
            if (request.getParameter(param) == null) {
                RequestDispatcher view;
                view = request.getRequestDispatcher("index.jsp");
                view.forward(request, response);
            } else {
                try {
                    hash.put(param, request.getParameter(param));
                    request.setAttribute(param, request.getParameter(param));
                    request.setAttribute("urrl", urrl);
                } catch (Exception e) {
                    System.out.println("something went wrong");
                }
            }

        }
        RequestDispatcher view = request.getRequestDispatcher("index.jsp");
        getRemainingSequences(hash);
    }
    /**
     * Runs the InputHandler.
     * @param parameters Hashmap
     */
    private void getRemainingSequences(final HashMap parameters) {
        InputHandler handle = new InputHandler();

        handle.runInputHandler(parameters);

        //String sequence = handle.runInputHandler(parameters);
    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        //processRequest(request, response);
        
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        processRequest(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
