/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import java.util.ArrayList;
import java.util.List;

/**
 * DONE:.
 * absence of hairpins >3bp
 * Restrict homopolymers (runs > 4)
 * Restrict dimers ( >* 4)
 *
 * @author dcdekker made all functions, removed some check style errors again
 * @author svanharen made script non-static and removed style errors.
 *
 */
public class PrimerChecker {

    List<String> sequences = new ArrayList<>();

    /**
     * Performs all checks in the PrimerChecker (used by KMaker) returns a.
     * boolean for each check
     *
     * @param kmer
     * @return boolean
     */
    public final boolean makeRestrict(final String kmer) {
        boolean returnment = false;
        if (restrictHomopolymers(kmer, 5) || restrictDimers(kmer) || restrictHairpins(kmer)) {
            returnment = true;
        }
        return returnment;

    }

    /**
     * Checks the sequence for homopolymers Uses regex and a limit to restrict
     * the amount of A,C,T, and G's. If more than 4 are present, a false will be
     * returned.
     * @param limit int
     * @param sequence String
     * @return false if the sequence contains homopolymer(s) longer than 4
     */
    public final Boolean restrictHomopolymers(final String sequence,
            final int limit) {
        String regex = "[ACTG]*([A]{" + limit + ",}|[C]{" + limit
                + ",}|[T]{" + limit + ",}|[G]{"
                + limit + ",})[ATCG]*";
        return !sequence.matches(regex);
    }

    /**
     * Checks the sequence for dimers of length longer than 4 Uses regex to
     * check the sequence for more than 4 Dimers. If 5 or more are found, a
     * false will be returned.
     * @param sequence String
     * @return false if the sequence contains Dimer(s) longer than 4
     */
    public final Boolean restrictDimers(final String sequence) {

        String regex = "[ACTG]*([ACTG]{2})\\1{4}[ACTG]*";
        return !sequence.matches(regex);

    }

    /**
     * Makes all possible subsequences from the sequence + the hairpin loop
     * which are possible in a hairpin construction, and compares them. Returns
     * a true if none of the subsequences matched. So no hairpin loops may be
     * formed.
     * @param sequence
     * @return true if no hairpins are present
     */
    public final boolean restrictHairpins(final String sequence) {
        String subsequence1;
        String subsequence2;

        int maxHairpinLength = sequence.length() - 6;

        //loops through the sequence
        for (int i = 0; i < sequence.length() - (maxHairpinLength + 2); i++) {

            // creates the minimum to maximum length hairpin possibilities
            for (int m = 4; m < sequence.length(); m++) {
                //limits the lengthening of hairpins by sequence length
                if (i + 2 + m + 3 < sequence.length()) {
                    subsequence1 = sequence.substring((i), (i + 3));
                    
                    subsequence2 = sequence.substring((i + 3 + m),
                            (i + 3 + m + 3));

                    if (checkComplement(subsequence1, subsequence2)) {
                        return false;

                    }
                }
            }
        }
        return true;
    }

    /**
     * Checks if the sequence is a reverse complement of the first sequence.
     * @param sequence
     * @param possibleComplement
     * @return true if sequence is a reverse compliment
     */
    private boolean checkComplement(final String sequence,
            final String possibleComplement) {
        String sequence1 = sequence.toUpperCase();
        StringBuilder comp = new StringBuilder();

        for (int i = 2; i >= 0; i--) {
            if (sequence1.charAt(i) == 'A') {
                comp.append("T");
            } else if (sequence1.charAt(i) == 'T') {
                comp.append("A");
            } else if (sequence1.charAt(i) == 'G') {
                comp.append("C");
            } else if (sequence1.charAt(i) == 'C') {
                comp.append("G");
            }
        }
        String complement = comp.toString();
        return !possibleComplement.equals(complement);
    }

}
