/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import bioinf.nl.teamprimer.kmaker.KMaker;
import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;

/**
 *
 * @author Dani2
 */
public class FileWriter {
    private PrintWriter writer;
    //private final String fileLocation =  System.getProperty("user.dir")+"/web/data/";
    public final String fileLocation =  "/homes/nmkeur/public_html/data/";
    public String zipFileLocation;


    /**
     * Creates a file with given length in the name.
     * @param length String
     */
    public final void createFile(final String length) {
        String filename = fileLocation + "kmers_of_length_" + length;

        try {
            writer = new PrintWriter(filename, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException ex) {
            System.out.println("something went wrong! Cannot write file");
        }

    }
    /**
     *
     * @return String fileLocation
     */
    public final String getFileLocation() {
        return fileLocation;
    }
    /**
     * @return String zipfile location
     */
    public String getZipFileLocation() {
        return zipFileLocation;
    }

    /**
     * Used to write the next file if/when limit has been reached.
     * Also closes the previous file.
     * @param length String
     * @param number Integer
     */
    public final void writeNextFile(final String length, final int number) {
        closeFile(length);
        String fileName = length + "_" + String.valueOf(number);
        createFile(fileName);
    }

    /**
     * Writes a sequence to the file.
     * @param sequence
     */
    public final void writeToFile(final String sequence) {
        writer.println(sequence);
        writer.println(makeComplement(sequence));

    }
    /**
     * Makes the complement of the given sequence.
     * @param sequence String
     * @return String sequence complement
     */
    private String makeComplement(final String sequence) {
        StringBuilder comSequence = new StringBuilder();
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == 'T') {
                comSequence.append('A');
            }
            if (sequence.charAt(i) == 'A') {
                comSequence.append('T');
            }
            if (sequence.charAt(i) == 'C') {
                comSequence.append('G');
            }
            if (sequence.charAt(i) == 'G') {
                comSequence.append('T');
            }
        }
        return comSequence.toString();
    }


    /**
    * Closes the file.
     * @param fileName
    */
    public final void closeFile(final String fileName) {
        writer.close();

    }
    /**
     * 
     * @param length
     * @param fileList 
     */
    public final void compressFiles(final String length,
            final ArrayList<String> fileList) {
        zipFileLocation = fileLocation + "kmers_of_length" + length + ".zip";
        KMaker km = new KMaker();
        km.kmerCompress(zipFileLocation, fileList);
    }

    public final String createCompleteFileName(final String length,
            final int secondCounter) {
        String fileName = fileLocation + "kmers_of_length"
                + length + "_" + String.valueOf(secondCounter) + ".txt";
        return fileName;
    }

    /**
    // just for testing at home without the zipped file
    public void returnStrings(){
        String length = "12";
        *
        createFile(length);
        *
        ArrayList<String> fileList = new ArrayList<String>();
        *
        //represents the inputhandler
        // is a loop that prints each sequence
        ArrayList<String> array = new ArrayList<>();
        array.add("start sequence");
        array.add("TGGGAGAGAG"); // 10 length
        array.add("TGGGGGGAAA");
        array.add("TGGGGGGAAA");
        array.add("TGGGGGGAAA");
        array.add("TGGGGGGAAA");
        array.add("end sequence");


        int counter = 0;
        int secondCounter = 0;
        for(String s : array){
            writeToFile(s);
            counter+=Integer.parseInt(length); //counts the characters used
            System.out.println(counter);

            if(counter >=265241152){ //265241152 is maximum amount of characters used
               secondCounter ++;
               writeNextFile(length,secondCounter);
               counter = 0;
               fileList.add( createCompleteFileName(length,secondCounter));
            }
        }
        closeFile(length);
        fileList.add(fileLocation+ "kmers_of_length_" +length);

        for(String s : fileList){
            System.out.println(s);
        }
        compressFiles(length, fileList);
    }
    **/
}
