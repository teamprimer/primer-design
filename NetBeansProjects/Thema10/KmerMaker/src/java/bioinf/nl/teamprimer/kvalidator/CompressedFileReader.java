/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.Enumeration;
import java.util.NoSuchElementException;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;

/**
 *
 * @author Sam van Haren
 *
 */
public class CompressedFileReader {
    /**
     * @param zipFileLocation for the location of the zipfile
     */
    private final String zipFileLocation;
    /**
     * @param zipFile is to use the zipfile Object easily.
     */
    private final ZipFile zipFile;
    /**
     * @param zipFile is to use the zipfile Object easily
     */
    private final Enumeration<? extends ZipEntry> entries;
    /**
     * @param zipInput a single zipInput, keeping it for i might need it.
     */
    private InputStream zipInput;
    /**
     * @param BFR because we need to read every time.
     */
    private BufferedReader bFR;
    /**
     * @param zipLocation the location of the zipfile
     * @throws IOException  for when no location of the zipfile is given.
     */
    public CompressedFileReader(final String zipLocation) throws IOException {
        this.zipFileLocation = zipLocation;
        this.zipFile = new ZipFile(this.zipFileLocation);
        this.entries = this.zipFile.entries();
        this.zipInput = zipFile.getInputStream(entries.nextElement());
        this.bFR = new BufferedReader(new InputStreamReader(zipInput, "UTF-8"));
    }
    /**
     *
     * @return the next line in a compressed text file.
     * @throws IOException for when no input is given
     */
    public final String giveLine() throws IOException {
        String returnment = null;
        String line = this.bFR.readLine();
        if (line == null) {
            try {
                this.bFR.close();
                this.zipInput = this.zipFile.getInputStream(
                        this.entries.nextElement());
                this.bFR = new BufferedReader(
                        new InputStreamReader(this.zipInput, "UTF-8"));
                returnment = this.bFR.readLine();
            } catch (NoSuchElementException ex) {
                this.zipInput.close();
                this.zipFile.close();
            }
        } else {
            returnment = line;
        }
        return returnment;
        }
    /**
     * closes the zip-file and the files within. (for when randomly stopping).
     * @throws IOException has to be.
     */
    public final void close() throws IOException {
        this.bFR.close();
        this.zipInput.close();
        this.zipFile.close();
    }
}
