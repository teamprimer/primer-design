/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kmaker;

import bioinf.nl.teamprimer.kvalidator.PrimerChecker;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipOutputStream;
/**
 *
 * @author Sam van Haren
 */
public class KMaker {
    /**
     * @param args the command line arguments
     * @throws java.io.IOException for when no args is given.
     */
    public static void main(final String[] args) throws IOException {
        /**
         * Initialize Function KStart to create Kmers.
         * and Write to File
         */
        KMaker k = new KMaker();
        List<String> kmers = k.kStart();
        k.firstWrite(kmers);
        k.otherKmers();
    }
    /**
     *Creates a list with possible combinations of Kmers till length 9.
     * And uses filters on this List to thin out the amount of Kmers.
     * @return list of Kmers.
     */
    public final List<String> kStart() {
        List<String> kmerList = new ArrayList<>();
        kmerList.add("A");
        kmerList.add("C");
        PrimerChecker pm = new PrimerChecker();
        int wall = Integer.parseInt("8");
        int i = kmerList.get(0).length();
        while (i != 0) {
            List<String> kpmerList = new ArrayList<>();
            if (i > wall) {
                i = 0;
            } else {
                for (String p : kmerList) {
                    kpmerList.add(p + "A");
                    kpmerList.add(p + "C");
                    kpmerList.add(p + "G");
                    kpmerList.add(p + "T");
                }
                List<String> tmp1 = new ArrayList<>(kpmerList);
                for (String s : kpmerList) {
                    if (!pm.makeRestrict(s)) {
                        tmp1.remove(s);
                    }
                }
            kmerList = tmp1;
            //System.out.println(KmerList.size());
            i = kmerList.get(kmerList.size() - 1).length();
                }
        }
    return kmerList;
    }
    /**
     *Starts writing to the first file of length 10 Kmers using length 9.
     * This also filters out Kmers just like in Kstart().
     * @param kmer An Arraylist of Kmers of length 9.
     */
    public final void firstWrite(final List<String> kmer) {
        List<String> kmerFiles = new ArrayList<>();
        PrimerChecker pm = new PrimerChecker();
        K_merFileMaker z = new K_merFileMaker();
        String fileLocation = "/commons/student/2014-2015/Thema10/svanharen/";
        z.fileOpen(fileLocation + Integer.toString(10));
        for (String s : kmer) {
            if (pm.makeRestrict(s + "A")) {
                    z.fileWrite(s + "A");
            }
            if (pm.makeRestrict(s + "C")) {
                    z.fileWrite(s + "C");
            }
            if (pm.makeRestrict(s + "G")) {
                    z.fileWrite(s + "G");
            }
            if (pm.makeRestrict(s + "T")) {
                    z.fileWrite(s + "T");
            }
        }
        kmerFiles.add(fileLocation + Integer.toString(10) + ".txt");
        z.fileClose();
        kmerCompress("/commons/student/2014-2015/Thema10/svanharen/"
                + Integer.toString(10) + ".zip", kmerFiles);
    }
    /**
     * Creates all the other Kmer Files using the files made before.
     * Starts by using the 10.txt made in firstWrite(), then follows with
     * using 11.txt when making 12.txt, in each file there are Kmers of the
     * given length.
     * @throws IOException for when missing input or output.
     */
    public final void otherKmers() throws IOException {
        int[] numbers = new int[6];
        numbers[0] = 11;
        numbers[1] = 12;
        numbers[2] = 13;
        numbers[3] = 14;
        numbers[4] = 15;
        numbers[5] = 16;
        PrimerChecker pm = new PrimerChecker();
        String fileLocation = "/commons/student/2014-2015/Thema10/svanharen/";
        for (int i : numbers) {
            List<String> kmerFiles = new ArrayList<>();
            int readNumber = i - 1;
            K_merFileMaker z = new K_merFileMaker();
            int part = 1;
            String lengthNumber = Integer.toString(i);
            String kmerFile = fileLocation + lengthNumber
                + Integer.toString(part) + ".txt";
            kmerFiles.add(kmerFile);
            String zipFile = fileLocation + lengthNumber + ".zip";
            int counter = 0;
            z.fileOpen(fileLocation + lengthNumber + Integer.toString(part));
            String[] listOfFiles = new File(fileLocation).list();
            for (String file : listOfFiles) {
                if (file.endsWith("txt")
                        && file.startsWith(Integer.toString(readNumber))) {
                    String readFile = fileLocation + file;
                    try (BufferedReader br = new BufferedReader(
                        new FileReader(readFile))) {
                        String line;
                        while ((line = br.readLine()) != null) {
                            if (pm.makeRestrict(line + "A")) {
                                    z.fileWrite(line + "A");
                            }
                            if (pm.makeRestrict(line + "C")) {
                                    z.fileWrite(line + "C");
                            }
                            if (pm.makeRestrict(line + "G")) {
                                    z.fileWrite(line + "G");
                            }
                            if (pm.makeRestrict(line + "T")) {
                                    z.fileWrite(line + "T");
                            }
                            counter += 4;
                            if (counter >= 265241152) {
                                counter = 0;
                                z.fileClose();
                                part += 1;
                                kmerFile = fileLocation + lengthNumber
                                    + Integer.toString(part) + ".txt";
                                z.fileOpen(fileLocation + lengthNumber
                                    + Integer.toString(part));
                                kmerFiles.add(kmerFile);
                            }
                        }
                    }
                    z.fileClose();
                }
            }
            kmerCompress(zipFile, kmerFiles);
        }
    }

    /**
     * Compresses a single file into a zip file to make storage usage less.
     * @param zipFile name of the zip file.
     * @param txtFiles name of the file that is going to be compressed.
     */
    public final void kmerCompress(final String zipFile,
            final List<String> txtFiles) {
            try (FileOutputStream fos = new FileOutputStream(zipFile)) {
                byte[] buffer = new byte[1024];
                try (ZipOutputStream zos = new ZipOutputStream(fos)) {
                    for (String txtFile : txtFiles) {
                        String fil = txtFile.split("/")[4];
                        ZipEntry ze = new ZipEntry(fil);
                        zos.putNextEntry(ze);
                        try (FileInputStream in =
                            new FileInputStream(txtFile)) {
                                int len;
                                while ((len = in.read(buffer)) > 0) {
                                    zos.write(buffer, 0, len);
                                }
                        }
                        zos.closeEntry();
                        zos.finish();
                    }
                }
            } catch (FileNotFoundException ex) {
            Logger.getLogger(KMaker.class.getName())
                    .log(Level.SEVERE, null, ex);
        } catch (IOException ex) {
            Logger.getLogger(KMaker.class.getName())
                    .log(Level.SEVERE, null, ex);
        }
    }
}
