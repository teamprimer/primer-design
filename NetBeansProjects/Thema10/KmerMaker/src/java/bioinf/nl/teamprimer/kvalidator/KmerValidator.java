/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package bioinf.nl.teamprimer.kvalidator;



/**
 * DONE:.
 * restrict maximum and/or minimum melting temperature
 * restrict sequence on CG-clamp
 * restrict diversity (% of AT- GC)
 * restrict on maximum palindrome length
 * restrict on an amount of PrimerDimers
 * restrict homopolymers(from PrimerChecker)
 *
 * @author dcdekker
 */
public class KmerValidator {

    /**
     * Uses the restrictHomoPolymers function from PrimerChecker Checks for
     * homopolymers, return true if found homopolymers are below limit Otherwise
     * returns a false.
     *
     * @param sequence
     * @param limit
     * @return boolean
     */
    public boolean restrictHomoPolymers(String sequence, int limit) {
        PrimerChecker pc = new PrimerChecker();
        if (pc.restrictHomopolymers(sequence, limit)) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Checks if the sequence contains Primer Dimers First checks if the
     * sequence contains any dimers, next makes every possible dimer, and uses
     * findMatchingDimers to find any matches. Returns false if more dimer
     * matches than the limit have been found, otherwise true
     *
     * @param sequence
     * @param limit
     * @return boolean
     */
    public final boolean restrictPrimerDimer(String sequence, int limit) {
        String regex = "[ACTG]*([ACTG]{2})[ACTG]*";
        int matches = 0;
        if (sequence.matches(regex)) {
            for (int i = 0; i < sequence.length() - 2; i++) {
                String s = Character.toString(sequence.charAt(i))
                        + Character.toString(sequence.charAt(i + 1));
                int position = i;
                if (findMatchingDimers(sequence, " ", position)) {
                    matches++;
                }
            }
        }
        if (matches / 2 > limit) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Finds any matching Dimers, if a match is found; returns a true. Reverses
     * the string, and checks for any matches for given Dimer.
     *
     * @param sequence
     * @param Dimer
     * @param position on string
     * @return boolean, true if match has been found
     */
    private boolean findMatchingDimers(String sequence, String Dimer, int position) {
        //reverse string
        StringBuilder seq = new StringBuilder();
        seq.append(sequence);
        String reverseSequence = seq.reverse().toString();
        String r = "";
        for (int j = position; j < reverseSequence.length() - 2; j++) {
            r = Character.toString(reverseSequence.charAt(j))
                    + Character.toString(reverseSequence.charAt(j + 1));
        }
        return Dimer.equals(r);
    }

    /**
     * Finds the longest palindrome in a sequence and compares it to given.
     * maximum length returns true if palindrome length is below given maximum
     *
     * @param sequence sequence
     * @param maxPalindromeLength maximum palindrome length
     * @return boolean, true if palindrome length is below the given maxiumum
     */
    public final Boolean restrictPalindromeLength(final String sequence,
            final int maxPalindromeLength) {
        String longest;
        longest = findLongestPalindrome(sequence);

        return longest.length() <= maxPalindromeLength;
    }

    /**
     * Uses findPalindrome to find the longest palindrome, Checks the entire
     * sequence using i and (i and i+1) as a center, passes it to findPalindrome
     * to create the longest palindromes for that position. All longest
     * palindromes are compared, and longest is returned.
     *
     * @param sequence kmer-sequence
     * @return String longest palindrome
     */
    private String findLongestPalindrome(final String sequence) {
        String longest = sequence.substring(0, 1);

        for (int i = 0; i < sequence.length(); i++) {
            // get longest palindrome with center of i
            String tmp = findPalindrome(sequence, i, i);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
            // get longest palindrome with center of i, i+1
            tmp = findPalindrome(sequence, i, i + 1);
            if (tmp.length() > longest.length()) {
                longest = tmp;
            }
        }
        return longest;
    }

    /**
     * Searches for a palindrome Uses a center,.
     * and elongates from there to both.
     * ends while 2 characters match,a longer palindrome is created longest
     * matching sequence will be returned
     *
     * @param sequence kmer sequence
     * @param begin start
     * @param end end
     * @return String longest found palindrome from position
     */
    private String findPalindrome(final String sequence,
            final int begin, final int end) {
        int begins = begin;
        int ends = end;
        while (begins >= 0 && ends <= sequence.length() - 1
                && sequence.charAt(begins) == sequence.charAt(ends)) {
            begins--;
            ends++;
        }
        return sequence.substring(begins + 1, ends);
    }

    /**
     * Calculates the percentage of AT based on occurrences returns a true if.
     * the percentage given matches, otherwise a false
     *
     * @param sequence kmer-sequence
     * @param diversity diversiteit
     * @return true if the AT percentage matches
     */
    public final boolean restrictSequenceDiversity(final String sequence,
            final double diversity) {

        double aTCount = 0;
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == 'A' | sequence.charAt(i) == 'T') {
                aTCount++;
            }
        }
        double sequenceDiversity = (aTCount / sequence.length()) * 100;
        if (sequenceDiversity >= diversity) {
            return true;
        } else {
            return false;
        }
    }

    /**
     * Returns True, if melting temperature is above the given minimum.
     * Otherwise returns a false.
     *
     * @param sequence
     * @param minMeltTemp
     * @return boolean
     */
    public boolean restrictMinimumMeltingTemperature(String sequence, double minMeltTemp) {
        double meltTemp = calculateMeltingTemperature(sequence);
        return meltTemp >= minMeltTemp;
    }

    /**
     * Returns True, if melting temperature is below the given maximum.
     * Otherwise returns a false.
     *
     * @param sequence
     * @param maxMeltTemp
     * @return boolean
     */
    public boolean restrictMaximumMeltingTemperature(String sequence, double maxMeltTemp) {
        double meltTemp = calculateMeltingTemperature(sequence);
        return meltTemp <= maxMeltTemp;
    }

    /**
     * Tests for a CG clamp Checks if the last 5 bases contain 1 to 3 C or G's.
     * and returns a true returns false otherwise
     * @param sequence
     * @return boolean
     */
    public Boolean checkCGClamp(String sequence) {

        String sub = sequence.substring(sequence.length() - 5);
        System.out.println(sub);
        if (sub.matches("[ACTG]*([CG]{4}|[AT]{5})[ACTG]*")) {
            return false;
        } else {
            return true;
        }
    }

    /**
     * Calculates the melting temperature based on one rule or the other
     * depending on the length of sequence source:
     * http://www.basic.northwestern.edu/biotools/oligocalc.html
     *
     * @param sequence
     * @return double melting temperature
     */
    private double calculateMeltingTemperature(String sequence) {
        int countA = 0;
        int countC = 0;
        int countG = 0;
        int countT = 0;
        double meltTemp;
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == 'A') {
                countA++;
            }
            if (sequence.charAt(i) == 'C') {
                countC++;
            }
            if (sequence.charAt(i) == 'T') {
                countT++;
            }
            if (sequence.charAt(i) == 'G') {
                countG++;
            }
        }
        if (sequence.length() < 14) {
            meltTemp = (countA + countT) * 2 + (countG + countC) * 4;
        } else {
            meltTemp = 64.9 + 41 * (countG + countC - 16.4) / (countA + countT + countG + countC);
        }
        return meltTemp;
    }

    /**
     * This function needs to be in a different class Translate the sequence to
     * make it complements form.
     *
     * @param sequence
     * @return comSequence
     */
    public String makeComplement(String sequence) {
        StringBuilder comSequence = new StringBuilder();
        for (int i = 0; i < sequence.length(); i++) {
            if (sequence.charAt(i) == 'T') {
                comSequence.append('A');
            }
            if (sequence.charAt(i) == 'A') {
                comSequence.append('T');
            }
            if (sequence.charAt(i) == 'C') {
                comSequence.append('G');
            }
            if (sequence.charAt(i) == 'G') {
                comSequence.append('T');
            }
        }
        return comSequence.toString();
    }
}
