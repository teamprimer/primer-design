<%-- 
    Document   : index
    Created on : Dec 11, 2014, 8:34:19 AM
    Author     : nmkeur
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html>

    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <link href="css/style.css" rel="stylesheet" type="text/css"/>
        <script src="js/lib/jquery.js" type="text/javascript"></script>
        <script src="js/lib/jquery.validate.min.js" type="text/javascript"></script>
        <script src="js/formValidator.js" type="text/javascript"></script>
        <title>PrimerMaker</title>
    </head>
    <body>



    <%@include file="includes/header.jsp" %>
    <%@include file="includes/form.jsp" %>
    <%@include file="includes/footer.jsp" %>
    
    
    </body>
</html>
