 
$(document).ready(function () {
    $('#submitform').validate({ // initialize the plugin
        rules: {
            length: {
                required: true,
                min: 10,
                max: 16
            },
            palindrome: {
                max: 5
            },
            Diversity:{
                min: 40
            },
            mintemp:{
                min: 40,
                max: 70
            },
            maxtemp:{
                min: 40,
                max: 70
            },
            primerdimer:{
            },
            homopolymers:{
            },
            gcclamp:{
            }
        },
            errorPlacement: function(){
            return false;  // suppresses error message text
        }
    });
    
$("#submitform").click(function(){
  $("d").hide();
});
});