<%-- 
    Document   : form
    Created on : Dec 11, 2014, 10:30:22 AM
    Author     : nmkeur
--%>

<form id ="submitform" action="submit" method="post" >

        <div id ="content">
            <h1>Welcome</h1>
            <p>This web based java application has been developed to create all possible kmers of a given length.</br>
                The only required field is the length of kmer all other options are optional and can be adjusted to users preferences. </br>
                It might to take a few moments to create the desired file. </br>
                Once ready a download option will appear.</p>
        <table>
        <tr>
            <td>Sequence Length: <img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter here the length of the sequence. The value must be between 10 and 16. Required parameter."></td>
            <td>
                <input name="length" type="text" placeholder="Length (10 to 16)"/></td>
            <td>(Required)</td>
        </tr>
        <tr>
            <td>Max Palindrome length: <img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter maximum value for palindrome.. Max is 5"></td>
            <td><input name="palindrome" type="text" placeholder="Max Palindrome 5"/></td>
        </tr>
        <tr>
            <td>Diversity: (AT)<img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter desired AT Diversity. Minimum value is 40%"></td>
            <td><input name="diversity" type="text" placeholder="Minimum value is 40% "/></td>
        </tr>
        <tr>
            <td>Min Melting temp:<img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter the minimum melting temperature.. Max is 40%"></td>
            <td><input name="mintemp" type="text" placeholder="Minimum is 40%"/></td>

        </tr>
        <tr>
            <td>Max Melting temp:<img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter the maximum melting temperature.. Max is 70%"></td>
            <td><input name="maxtemp" type="text" placeholder="Maximum is 70%"/></td>
        </tr>
        <tr>
            <td>Maximum Primer Dimer: <img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter maximum value of primer dimer."></td>
            <td><input name="primerdimer" type="text" placeholder="Enter maximum primer dimer"alt="HOI"</td>
        </tr>
        <tr>
            <td>Maximum Homopolymers: <img class="iconresize" src="images/toolbar_get_info.png"
            title="Enter maximum value of homopolymers."></td>
            <td><input name="homopolymers" type="text" placeholder="Enter maximum polymers"</td>
        </tr>
        <tr>
            <td>GC Clamp: <img class="iconresize" src="images/toolbar_get_info.png"
            title="Select yes to restrict kmers to contain a GC Clamp.. Default is NO"></td>
            <td>Yes<input name="gcclamp" type="radio" value="yes"/>
        </tr>
        
        </table>
        <button type="submit" value="submit"> Submit</button>
        <d><a href=${requestScope.urrl}> Download </a></d>
        </div>
    
</form>
